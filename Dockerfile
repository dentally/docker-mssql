FROM mcr.microsoft.com/mssql-tools

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    libpq-dev \
    nodejs \
    libc6-dev \
    wget \
    curl \
    apt-transport-https \
    debconf-utils \
    ruby-full \
    && rm -rf /var/lib/apt/lists/*

RUN wget ftp://ftp.freetds.org/pub/freetds/stable/freetds-1.00.27.tar.gz && \
    tar -xzf freetds-1.00.27.tar.gz && \
    cd freetds-1.00.27 && \
    ./configure --prefix=/usr/local --with-tdsver=7.3 && \
    make && \
    make install

RUN gem install tiny_tds csv
RUN /sbin/ldconfig /usr/local/lib
